require 'rss'
require 'time'
require 'optparse'
require 'ostruct'

@options = OpenStruct.new
OptionParser.new do |opt|
  @options.url = ''
  @options.begin_feed = 'BEGIN_FEED'
  @options.end_feed = 'END_FEED'
  @options.file = 'README.md'
  @options.count = 4

  opt.on('-u', '--url URL', 'The RSS URL') { |o| @options.url = o }
  opt.on('-b', '--begin BEGIN_FEED', 'Beginning marker') { |o| @options.begin_feed = o }
  opt.on('-e', '--end END_FEED', 'Ending marker') { |o| @options.end_feed = o }
  opt.on('-f', '--file FILE', 'The file to insert into') { |o| @options.file = o }
  opt.on('-c', '--count NUMBER', Integer, 'Number of items to insert') { |o| @options.count = o}
end.parse!

url = @options.url; puts "URL: #{url}"
begin_feed = "<!---#{@options.begin_feed}--->\n"; puts "Begin: #{begin_feed}"
end_feed = "<!---#{@options.end_feed}--->"; puts "End: #{end_feed}"
feed_regex = /#{begin_feed}(.*)#{end_feed}/im 
README = @options.file; puts "File: #{README}"
exit(1) if url == ''

# Read the feed
entries = 0
feed_string = begin_feed
feed_string += "|Published link | Description|\n"
feed_string += "|--|--|\n"
URI.open(url) do |rss|
  feed = RSS::Parser.parse(rss)
  feed.items.each do |item|
    summary = "|[#{item.pubDate.iso8601}](#{item.link}) | #{item.description}|\n"
    summary = summary.gsub('T',' T').gsub('+00:00','Z')
    feed_string += summary 
    puts summary
    entries += 1
    break if entries >= @options.count
  end
end
feed_string += end_feed

# Load the README
readme_file = File.open(README)
readme_buffer = readme_file.read
readme_file.close

# Insert the feed
File.write(README, readme_buffer.gsub(feed_regex, feed_string))
