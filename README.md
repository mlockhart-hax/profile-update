# 👋 G'day, I'm Mike Lockhart 

Computer generalist, percussionist, humanist, and dad. Sometimes also a computer-percussionist 💻🔨

I make computers work for people, and I'm lucky enough to do it for [my day job](https://gitlab.com/mlockhart).
I like to help all kinds of people with technology stuff. 

Here are some links to info about me:

- 📇 My online business card is https://about.me/mikelockhart
- 🐘 <a rel="me" href="https://mastodon.au/@milohax">Mastodon AU</a>, <a rel="me" href="https://hachyderm.io/@milohax">Hachyderm.io</a>
- 💻 [Public SSH keys on GitLab.com](https://gitlab.com/milohax.keys)
- 🔐 [Public PGP key 3CCA2E6EBCBE8795](https://keybase.io/sinewalker/key.asc)
- 🐙 I also have [a GitHub account](https://github.com/sinewalker)

Do get in touch with me for meet-ups online and around Hobart (my timezone: UTC+10).

## 📔 Latest posts

<!---BEGIN_FEED--->

<!---END_FEED--->
